# Build system requires too many stuff just to get the git hash of the
# tag used for building the tar.gz. Setting here.
%global commit c0fb784eebf9d36a67c736d0428fb3577f2e25bb

Name:           unboundid-ldapsdk
Version:        4.0.14
Release:        2
Summary:        UnboundID LDAP SDK for Java

License:        GPLv2 or LGPLv2+
URL:            https://www.ldap.com/unboundid-ldap-sdk-for-java
Source0:        https://github.com/pingidentity/ldapsdk/archive/%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  ant
BuildRequires:  maven-local

%description
The UnboundID LDAP SDK for Java is a fast, powerful, user-friendly, and
completely free Java library for communicating with LDAP directory servers and
performing related tasks like reading and writing LDIF, encoding and
decoding data using base64 and ASN.1 BER, and performing secure communication.

%package javadoc
Summary:        Javadoc for %{name}

%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n ldapsdk-%{version}
# remove external prebuilt libraries
rm -rf ext

%build
# checkstyle configuration files were in the ext directory
# code doesn't pass checkstyle 5.6 disabling the check until upstream
# fixes it
# upstream added code for getting exact git references for the build
# too many deps just for getting a git hash.

ant \
    -Dcheckstyle.enabled=false \
    -Drepository-info.revision=%{commit} \
    -Drepository-info.revision-number=-1 \
    -Drepository-info.type=git \
    -Drepository-info.url=https://github.com/pingidentity/ldapsdk.git \
    -Depository-info.path=\/ \
    -f build.xml package

%install

%jar -xf build/package/%{name}-%{version}-maven.jar
rm -fr META-INF
mkdir javadoc
cd javadoc
%jar -xf ../unboundid-ldapsdk-*-javadoc.jar
rm -fr META-INF
cd ..

%mvn_artifact %{name}-%{version}.pom %{name}-%{version}.jar
%mvn_file com.unboundid:%{name} %{name}
%mvn_install -J javadoc

%files -f .mfiles
%license dist-root/LICENSE.txt
%license dist-root/LICENSE-LGPLv2.1.txt
%license dist-root/LICENSE-UnboundID-LDAPSDK.txt
%license dist-root/LICENSE-GPLv2.txt
%doc dist-root/README.txt

%files javadoc -f .mfiles-javadoc
%license dist-root/LICENSE.txt
%license dist-root/LICENSE-LGPLv2.1.txt
%license dist-root/LICENSE-UnboundID-LDAPSDK.txt
%license dist-root/LICENSE-GPLv2.txt

%changelog
* Tue Mar 05 2024 yanjianqing <yanjianqing@kylinos.cn> - 4.0.14-2
- Initial package
